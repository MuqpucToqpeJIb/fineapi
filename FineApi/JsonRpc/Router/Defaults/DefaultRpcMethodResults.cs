﻿using FineApi.JsonRpc;
using FineApi.JsonRpc.Router.Abstractions;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FineApi.JsonRpc.Router.Defaults
{
	/// <summary>
	/// Error result for rpc responses
	/// </summary>
	public class RpcMethodErrorResult<TMessage, TData> : IRpcMethodResult
	{
		/// <summary>
		/// Error message
		/// </summary>
		public TMessage Message { get; }
		/// <summary>
		/// JSON-RPC error code
		/// </summary>
		public int ErrorCode { get; }

		/// <summary>
		/// Data for error response
		/// </summary>
		public TData Data { get; }

		/// <param name="errorCode">JSON-RPC error code</param>
		/// <param name="message">(Optional)Error message</param>
		/// <param name="data">(Optional)Data for error response</param>
		public RpcMethodErrorResult(int errorCode, TMessage message = default(TMessage), TData data = default(TData))
		{
			this.ErrorCode = errorCode;
			this.Message = message;
			this.Data = data;
		}

		/// <summary>
		/// Turns result data into a rpc response
		/// </summary>
		/// <param name="id">Rpc request id</param>
		/// <param name="serializer">Json serializer function to use for objects for the response</param>
		/// <returns>Rpc response for request</returns>
		public RpcResponse ToRpcResponse(object id, Func<object, JToken> serializer)
		{
			JToken message = this.Message == null ? null : serializer(this.Message);
			JToken data = this.Data == null ? null : serializer(this.Data);
            RpcError error = new RpcError(this.ErrorCode, message, data);
			return new RpcResponse(id, error);
		}
	}

	/// <summary>
	/// Success result for rpc responses
	/// </summary>
	public class RpcMethodSuccessResult<TReturn> : IRpcMethodResult
	{
		/// <summary>
		/// Object to return in rpc response
		/// </summary>
		public TReturn ReturnObject { get; }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="returnObject">Object to return in rpc response</param>
		public RpcMethodSuccessResult(TReturn returnObject = default(TReturn))
		{
			this.ReturnObject = returnObject;
		}

		/// <summary>
		/// Turns result data into a rpc response
		/// </summary>
		/// <param name="id">Rpc request id</param>
		/// <param name="serializer">Json serializer function to use for objects for the response</param>
		/// <returns>Rpc response for request</returns>
		public RpcResponse ToRpcResponse(object id, Func<object, JToken> serializer)
		{
			JToken jToken = this.ReturnObject == null ? null : serializer(this.ReturnObject);
			return new RpcResponse(id, jToken);
		}
	}
}
