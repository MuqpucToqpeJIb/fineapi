﻿using FineApi.JsonRpc.Router.Defaults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FineApi.JsonRpc.Router
{
	public abstract class RpcController
	{
		/// <summary>
		/// Helper for returning a successful rpc response
		/// </summary>
		/// <param name="obj">Object to return in response</param>
		/// <returns>Success result for rpc response</returns>
		public RpcMethodSuccessResult<T> Ok<T>(T obj = default(T))
		{
			return new RpcMethodSuccessResult<T>(obj);
		}


        /// <summary>
        /// Helper for returning an error rpc response
        /// </summary>
        /// <param name="errorCode">JSON-RPC custom error code</param>
        /// <param name="message">(Optional)Error message</param>
        /// <param name="data">(Optional)Error data</param>
        /// <returns></returns>
        public RpcMethodErrorResult<TMessage, TData> Error<TMessage, TData>(int errorCode, TMessage message, TData data)
        {
            return new RpcMethodErrorResult<TMessage, TData>(errorCode, message, data);
        }
        /// <summary>
        /// Helper for returning an error rpc response
        /// </summary>
        /// <param name="errorCode">JSON-RPC custom error code</param>
        /// <param name="message">(Optional)Error message</param>
        /// <param name="data">(Optional)Error data</param>
        /// <returns></returns>
        public RpcMethodErrorResult<TMessage, object> Error<TMessage>(int errorCode, TMessage message = default(TMessage))
        {
            return new RpcMethodErrorResult<TMessage, object>(errorCode, message);
        }
        /// <summary>
        /// Helper for returning an error rpc response
        /// </summary>
        /// <param name="errorCode">JSON-RPC custom error code</param>
        /// <param name="message">(Optional)Error message</param>
        /// <param name="data">(Optional)Error data</param>
        /// <returns></returns>
        public RpcMethodErrorResult<string, object> Error(int errorCode, string message)
        {
            return new RpcMethodErrorResult<string, object>(errorCode, message);
        }
        /// <summary>
        /// Helper for returning an error rpc response
        /// </summary>
        /// <param name="errorCode">JSON-RPC custom error code</param>
        /// <param name="message">(Optional)Error message</param>
        /// <param name="data">(Optional)Error data</param>
        /// <returns></returns>
        public RpcMethodErrorResult<object, object> Error(int errorCode)
        {
            return new RpcMethodErrorResult<object, object>(errorCode);
        }
    }


#if !NETSTANDARD1_3
	/// <summary>
	/// Attribute to decorate a derived <see cref="RpcController"/> class
	/// </summary>
	public class RpcRouteAttribute : Attribute
	{
		/// <summary>
		/// Name of the route to be used in the router. If unspecified, will use controller name.
		/// </summary>
		public string RouteName { get; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="routeName">(Optional) Name of the route to be used in the router. If unspecified, will use controller name.</param>
		/// <param name="routeGroup">(Optional) Name of the group the route is in to allow route filtering per request.</param>
		public RpcRouteAttribute(string routeName = null)
		{
			this.RouteName = routeName?.Trim();
		}
	}
#endif
}
