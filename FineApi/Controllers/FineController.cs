﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FineApi.Models;
using Microsoft.AspNetCore.Mvc;
using FineApi.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Newtonsoft.Json;

namespace FineApi.Controllers
{
    [ApiVersionNeutral]
    [Produces("application/json")]
    [Route("api/Fines")]
    public class FineController: Controller
    {
        private readonly IFineRepository _repository;
        private readonly IUploadRepository _upload;
        public FineController(IFineRepository repository, IUploadRepository upload)
        {
            this._repository = repository;
            this._upload = upload;
        }
        //GET: api/Fines

        [HttpGet]
        public IEnumerable<Fine> FetchFines()
        {
            return _repository.FetchFines();
        }

        [HttpGet("/api/Fines/{id}")]
        public Fine FetchFine([FromRoute] int id)
        {
            return _repository.FetchFine(id);
        }
        //POST: api/Fines
        [HttpPost]
        public async Task<FineResult> CreateFine([FromBody] Fine fine)
        {
           var result = await _repository.CreateFine(fine);
           
            return result;
        }

        [HttpPost("/upload/img/{number}/{id}")]
        public async Task<string> UploadImg(List<IFormFile> file, [FromRoute] string number, [FromRoute] int id)
        {
           var result = await this._upload.Upload(file, number);
           var photo =  await _repository.UpdatePhoto(id, result);
            return photo;
        }
    }
}