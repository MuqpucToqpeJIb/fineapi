﻿using FineApi.Data;
using FineApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace FineApi.Controllers
{
    [ApiVersionNeutral]
    [Produces("application/json")]
    [Route("api/check")]
    public class CheckController : Controller
    {
        private readonly ICheckRepository _repository;
        public CheckController(ICheckRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("/api/checkpark/{number}")]
        public CheckParkingModel CheckParking([FromRoute] string number)
        {
            return _repository.CheckParking(number);
        }
    }
}