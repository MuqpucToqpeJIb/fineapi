﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FineApi.Data;
using FineApi.JsonRpc.Router;
using FineApi.JsonRpc.Router.Abstractions;
using Microsoft.AspNetCore.Mvc;
using FineApi.Entity.Enum;
using FineApi.Extensions;
using FineApi.Infrastructure.Authorization.Cashbox;
using FineApi.Manager.Interfaces;
using FineApi.Models;


namespace FineApi.Controllers
{
    [ApiVersionNeutral]
    [Route("[controller]")]
    public class PaycomController : RpcController
    {
        private readonly IPaymentRepository _paymentRepository;
        /*private readonly IPeopleManager _peopleManager;
        private readonly IRequestManager _requestManager;
        private readonly ITransactionManager _transactionManager;
        private readonly IRequestTransactionManager _requestTransactionManager;
        private readonly IAppSettingsManager _appSettingsManager;*/

        public PaycomController(IPaymentRepository paymentRepository)
        {
            _paymentRepository = paymentRepository;
           /* _peopleManager = peopleManager;
            _requestManager = requestManager;
            _transactionManager = transactionManager;
            _requestTransactionManager = requestTransactionManager;
            _appSettingsManager = appSettingsManager;*/
        }

        /// <summary>
        /// Проверка возможности создания финансовой транзакции
        /// </summary>
        /// <param name="amount">Сумма платежа (в тийинах).</param>
        /// <param name="account">Счет потребителя услуг.</param>
        /// <returns></returns>
        /// <example>
        ///{
        ///    "method" : "CheckPerformTransaction",
        ///    "params" : {
        ///        "amount" : 500000,
        ///        "account" : {
        ///            "card" : "7700 0000 0000 0001"
        ///        }
        ///    }
        ///}
        /// </example>
        public IRpcMethodResult CheckPerformTransaction(int amount, AccountModel account)
        {
           /* // Проверка суммы - -31001
            if (amount < 50000 || amount > 100000000)
            {
                return Error(-31001, CashboxErrorMessage.WrongSum());
            }
            // Поиск пользователя по номеру телефона
            var people = await _peopleManager.GetByPhoneMobile(account.Phone);
            if (people == null)
            {
                return Error(-31050, CashboxErrorMessage.NoPhoneFound());
            }*/
            return  Ok(new CheckPerformTransactionResult { Allow = true });
        }

        /// <summary>
        /// Создание финансовой транзакции
        /// </summary>
        /// <param name="id">Идентификатор транзакции Paycom.</param>
        /// <param name="time">Время создания транзакции Paycom.</param>
        /// <param name="amount">Сумма платежа (в тийинах).</param>
        /// <param name="account">Счет потребителя услуг.</param>
        /// <returns></returns>
        /// <example>
        /// {
        ///    "method" : "CreateTransaction",
        ///    "params" : {
        ///        "id" : "5305e3bab097f420a62ced0b",
        ///        "time" : 1399114284039,
        ///        "amount" : 500000,
        ///        "account" : {
        ///            "number" : "01us250"
        ///        }
        ///    }
        /// }
        /// </example>
        public async Task<IRpcMethodResult> CreateTransaction(string id, long time, int amount, Payment account)
        {
            account.CreateTime = DateTime.UtcNow;
            account.Amount = amount;
            account.Transaction = id;
            if (string.IsNullOrWhiteSpace(id))
            {
                return Error(-31008, CashboxErrorMessage.NoTransactionId());
            }
            // Проверка суммы - -31001
            if (amount < 50000 || amount > 100000000)
            {
                return Error(-31001, CashboxErrorMessage.WrongSum());
            }
            // Поиск транзакции по Идентификатору
           var requestTransaction = await _paymentRepository.GetById(id);
            if (requestTransaction == null)
           {
                
                var requestsTransactions = await _paymentRepository.GetById(id);
                if (requestsTransactions != null)
                {
                    return Error(-31099, CashboxErrorMessage.PendingPayment());
                }
               requestTransaction = await _paymentRepository.CreatePayment(account);
            }
            if (requestTransaction.Status != TransactionStatus.Pending)
            {
                return Error(-31008, CashboxErrorMessage.TransactionStatusError());
            }
            /*if (requestTransaction.CreateTime < DateTime.Now.AddMinutes(-2))
            {
                return Error(-31008, CashboxErrorMessage.TransactionStatusError());
            }*/
            return Ok(new CreateTransactionResult
            {
                CreateTime = account.CreateTime,
                State = TransactionStatus.Pending,
                Transaction = account.Id.ToString()
            });
        }

        /// <summary>
        /// Проведение финансовой транзакции
        /// </summary>
        /// <param name="id">Идентификатор транзакции Paycom.</param>
        /// <returns></returns>
        /// <example>
        ///{
        ///    "method" : "PerformTransaction",
        ///    "params" : {
        ///        "id" : "5305e3bab097f420a62ced0b"
        ///    }
        ///}
        /// </example>
        public async Task<IRpcMethodResult> PerformTransaction(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return Error(-31003, CashboxErrorMessage.NoTransaction());
            }
            // Поиск транзакции по Идентификатору
            var requestTransaction = await _paymentRepository.GetById(id);
            if (requestTransaction == null)
            {
                return Error(-31003, CashboxErrorMessage.NoTransaction());
            }
            if (requestTransaction.Status == TransactionStatus.Pending)
            {
                /*if (requestTransaction.CreateTime < DateTime.Now.AddMinutes(-2))
                {
                    requestTransaction = await _requestTransactionManager.Cancel(id, CancelReason.CanceledByTimeout);
                    return Error(-31008, CashboxErrorMessage.TransactionStatusError());
                }*/
                requestTransaction = await _paymentRepository.Perform(id);
            }
            else if (requestTransaction.Status != TransactionStatus.Confirmed)
            {
                return Error(-31008, CashboxErrorMessage.TransactionStatusError());
            }
            return Ok(new PerformTransactionResult
            {
                PerformTime = requestTransaction.PerformTime ?? DateTimeExtension.UnixStartTimestampDate,
                State = (int)requestTransaction.Status,
                Transaction = requestTransaction.Id.ToString()
            });
        }

        /// <summary>
        /// Отмена финансовой транзакции
        /// </summary>
        /// <param name="id">Идентификатор транзакции Paycom.</param>
        /// <param name="reason">Причина отмены транзакции.</param>
        /// <returns></returns>
        /// <example>
        ///{
        ///    "method" : "CancelTransaction",
        ///    "params" : {
        ///        "id" : "5305e3bab097f420a62ced0b",
        ///        "reason" : 1
        ///    }
        ///}
        /// </example>
        public async Task<IRpcMethodResult> CancelTransaction(string id, CancelReason reason)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return Error(-31003, CashboxErrorMessage.NoTransaction());
            }
            // Поиск транзакции по Идентификатору
            var requestTransaction = await _paymentRepository.GetById(id);
            if (requestTransaction == null)
            {
                return Error(-31003, CashboxErrorMessage.NoTransaction());
            }
            if (requestTransaction.Status == TransactionStatus.Pending)
            {
                requestTransaction = await _paymentRepository.Cancel(id, reason);
            }
            else if (requestTransaction.Status == TransactionStatus.Confirmed)
            {
                //TODO Реализовать проверку баланса до отмены подтвержденной транзакции
                requestTransaction = await _paymentRepository.Cancel(id, reason);
            }
            return Ok(new CancelTransactionResult
            {
                CancelTime = requestTransaction.CancelTime ?? DateTimeExtension.UnixStartTimestampDate,
                State = (int)requestTransaction.Status,
                Transaction = requestTransaction.Id.ToString()
            });
        }

        /// <summary>
        /// Проверка состояния финансовой транзакции
        /// </summary>
        /// <param name="id">Идентификатор транзакции Paycom.</param>
        /// <returns></returns>
        /// <example>
        ///{
        ///    "method" : "CheckTransaction",
        ///    "params" : {
        ///        "id" : "5305e3bab097f420a62ced0b"
        ///    }
        ///}
        /// </example>
        public async Task<IRpcMethodResult> CheckTransaction(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return Error(-31003, CashboxErrorMessage.NoTransaction());
            }
            // Поиск транзакции по Идентификатору
            var requestTransaction = await _paymentRepository.GetById(id);
            if (requestTransaction == null)
            {
                return Error(-31003, CashboxErrorMessage.NoTransaction());
            }
            return Ok(new CheckTransactionResult
            {
                CancelTime = requestTransaction.CancelTime ?? DateTimeExtension.UnixStartTimestampDate,
                PerformTime = requestTransaction.PerformTime ?? DateTimeExtension.UnixStartTimestampDate,
                CreateTime = requestTransaction.CreateTime,
                Transaction = requestTransaction.Id.ToString(),
                State = (int)requestTransaction.Status,
                Reason = requestTransaction.Reason
            });
        }

        /*/// <summary>
        /// Информация о транзакциях мерчанта
        /// </summary>
        /// <param name="from">Начало периода.</param>
        /// <param name="to">Окончание периода.</param>
        /// <returns></returns>
        /// <example>
        ///{
        ///    "method" : "GetStatement",
        ///    "params" : {
        ///        "from" : 1399114284039,
        ///        "to" : 1399120284000
        ///    }
        ///}
        /// </example>*/
        /*public Task<IRpcMethodResult> GetStatement(long from, long to)
        {
            // Поиск транзакции по заданному периоду
            var requestsTransactions =  _paymentRepository.GetByPeriod(from, to);
            return Ok(new GetStatementResult<AccountModel>
            {
                Transactions = requestsTransactions.Select(tr => new TransactionModel<AccountModel>
                {
                    Id = tr.TransactionId.ToString(),
                    Account = new AccountModel
                    {
                        Number = tr.Number
                    },
                    Amount = tr.Amount,
                    CancelTime = tr.ProviderCancelTime ?? DateTimeExtension.UnixStartTimestampDate,
                    CreateTime = tr.ProviderCreateTime,
                    PerformTime = tr.ProviderPerformTime ?? DateTimeExtension.UnixStartTimestampDate,
                    Transaction = tr.Id.ToString(),
                    State = tr.ProviderStatus,
                    Time = tr.ProviderCreateTime
                })
            });
        }*/
/*
        /// <summary>
        /// Изменение пароля
        /// </summary>
        /// <param name="password">Пароль</param>
        /// <returns></returns>
        public async Task<IRpcMethodResult> ChangePassword(string password)
        {
            try
            {
                _appSettingsManager.UpdateCashboxOptions(password);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return Ok(new ChangePasswordResult { Success = true });
        }*/
    }
}