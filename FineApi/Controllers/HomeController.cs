﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using FineApi.Data;
using Microsoft.AspNetCore.Mvc;
using FineApi.Models;

namespace FineApi.Controllers
{
    [ApiVersionNeutral]
    public class HomeController : Controller
    {
        private readonly IFineRepository _repository;
        private readonly IUploadRepository _upload;
        
        public HomeController(IFineRepository repository, IUploadRepository upload)
        {
            this._repository = repository;
            this._upload = upload;
        }
        
        public ActionResult Index()
        {
            var result = _repository.FetchFines().ToList();
            return View(result);
        }
        
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}