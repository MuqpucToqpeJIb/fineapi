﻿using FineApi.Manager.Interfaces;
using FineApi.Store.Interfaces;

namespace FineApi.Manager
{
    public class RequestManager : IRequestManager
    {
        private readonly IRequestStore _requestStore;

        public RequestManager(IRequestStore requestStore)
        {
            _requestStore = requestStore;
        }
    }
}