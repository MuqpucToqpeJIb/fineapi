﻿using System.Threading.Tasks;
using FineApi.Entity;

namespace FineApi.Manager.Interfaces
{
    public interface IPeopleManager
    {
        /// <summary>
        /// Возвращает пользователя по его номеру мобильного телефона
        /// </summary>
        /// <param name="phoneMobile">Номер мобильного телефона</param>
        /// <returns></returns>
        Task<People> GetByPhoneMobile(string phoneMobile);
    }
}