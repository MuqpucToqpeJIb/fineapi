﻿
namespace FineApi.Manager.Interfaces
{
    public interface IAppSettingsManager
    {
        /// <summary>
        /// Обновляет секцию CashboxOptions в файле appsettings.json
        /// </summary>
        /// <param name="key">Значение свойства Key в сексии CashboxOptions</param>
        void UpdateCashboxOptions(string key);
    }
}