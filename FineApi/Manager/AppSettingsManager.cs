﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using FineApi.Manager.Interfaces;

namespace FineApi.Manager
{
    public class AppSettingsManager : IAppSettingsManager
    {
        private IConfiguration _configuration;
        private readonly string filePath;

        public AppSettingsManager(IConfiguration configuration)
        {
            _configuration = configuration;
            filePath = Directory.GetCurrentDirectory() + "\\appsettings.json";
        }

        /// <inheritdoc />
        /// <summary>
        /// Обновляет секцию CashboxOptions в файле appsettings.json
        /// </summary>
        /// <param name="key">Значение свойства Key в сексии CashboxOptions</param>
        public void UpdateCashboxOptions(string key)
        {
            try
            {
                _configuration["CashboxOptions:Key"] = key;

                var jsonStr = File.ReadAllText(filePath);
                dynamic jsonObj = JsonConvert.DeserializeObject(jsonStr);
                jsonObj["CashboxOptions"]["Key"] = key;
                var result = JsonConvert.SerializeObject(jsonObj, Formatting.Indented);
                File.WriteAllText(filePath, result);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}