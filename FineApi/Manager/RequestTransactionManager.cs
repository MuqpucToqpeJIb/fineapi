﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FineApi.Entity;
using FineApi.Entity.Enum;
using FineApi.Manager.Interfaces;
using FineApi.Store.Interfaces;

namespace FineApi.Manager
{
    public class RequestTransactionManager : IRequestTransactionManager
    {
        private readonly IRequestTransactionStore _requestTransactionStore;

        public RequestTransactionManager(IRequestTransactionStore requestTransactionStore)
        {
            _requestTransactionStore = requestTransactionStore;
        }

        /// <inheritdoc />
        /// <summary>
        /// Возвращает paycom запрос и транзакцию по идентификатору транзакции, провайдера
        /// </summary>
        /// <param name="id">Идентификатор транзакции, провайдера</param>
        /// <returns></returns>
        public async Task<RequestTransaction> GetById(string id)
        {
            return await _requestTransactionStore.GetById(id);
        }

        /// <inheritdoc />
        /// <summary>
        /// Создает paycom запрос и транзакцию
        /// </summary>
        /// <param name="id">Идентификатор транзакции, провайдера</param>
        /// <param name="peopleId">Идентификатор пользователя</param>
        /// <param name="createdAt">Время создания транзакции провайдером</param>
        /// <param name="amount">Сумма платежа</param>
        /// <returns></returns>
        public async Task<RequestTransaction> Create(string id, long peopleId, DateTime createdAt, int amount)
        {
            return await _requestTransactionStore.Create(id, peopleId, createdAt, amount);
        }

        /// <inheritdoc />
        /// <summary>
        /// Отмена paycom запроса и транзакции
        /// </summary>
        /// <param name="id">Идентификатор транзакции, провайдера</param>
        /// <param name="reason">Причина отмены транзакции</param>
        /// <returns></returns>
        public async Task<RequestTransaction> Cancel(string id, CancelReason reason)
        {
            return await _requestTransactionStore.Cancel(id, reason);
        }

        /// <inheritdoc />
        /// <summary>
        /// Подтверждение paycom запроса и транзакции
        /// </summary>
        /// <param name="id">Идентификатор транзакции, провайдера</param>
        /// <returns></returns>
        public async Task<RequestTransaction> Perform(string id)
        {
            return await _requestTransactionStore.Perform(id);
        }

        /// <inheritdoc />
        /// <summary>
        /// Отмена paycom запроса и транзакции после подтверждения
        /// </summary>
        /// <param name="id">Идентификатор транзакции, провайдера</param>
        /// <param name="reason">Причина отмены транзакции</param>
        /// <returns></returns>
        public async Task<RequestTransaction> CancelAfterConfirm(string id, CancelReason reason)
        {
            return await _requestTransactionStore.CancelAfterConfirm(id, reason);
        }

        /// <inheritdoc />
        /// <summary>
        /// Возвращает список paycom запросов и транзакций за период
        /// </summary>
        /// <param name="dateFrom">Дата начала периода</param>
        /// <param name="dateTo">Дата окончания периода</param>
        /// <returns></returns>
        public async Task<IEnumerable<RequestTransaction>> GetByPeriod(DateTime dateFrom, DateTime dateTo)
        {
            return await _requestTransactionStore.GetByPeriod(dateFrom, dateTo);
        }

        /// <inheritdoc />
        /// <summary>
        /// Возвращает список список paycom запросов и транзакций пользователя по статусу
        /// </summary>
        /// <param name="peopleId">Идентификатор пользователя</param>
        /// <param name="status">Статус транзакции</param>
        /// <returns></returns>
        public async Task<IEnumerable<RequestTransaction>> GetByPeopleIdAndStatus(long peopleId, TransactionStatus status)
        {
            return await _requestTransactionStore.GetByPeopleIdAndStatus(peopleId, status);
        }
    }
}