﻿using System.Threading.Tasks;
using FineApi.Entity;
using FineApi.Manager.Interfaces;
using FineApi.Store.Interfaces;

namespace FineApi.Manager
{
    public class PeopleManager : IPeopleManager
    {
        private readonly IPeopleStore _peopleStore;

        public PeopleManager(IPeopleStore peopleStore)
        {
            _peopleStore = peopleStore;
        }

        /// <inheritdoc />
        /// <summary>
        /// Возвращает пользователя по его номеру мобильного телефона
        /// </summary>
        /// <param name="phoneMobile">Номер мобильного телефона</param>
        /// <returns></returns>
        public async Task<People> GetByPhoneMobile(string phoneMobile)
        {
            return await _peopleStore.GetByPhoneMobile(phoneMobile);
        }
    }
}