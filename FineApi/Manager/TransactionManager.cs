﻿using FineApi.Manager.Interfaces;
using FineApi.Store.Interfaces;

namespace FineApi.Manager
{
    public class TransactionManager : ITransactionManager
    {
        private readonly ITransactionStore _transactionStore;

        public TransactionManager(ITransactionStore transactionStore)
        {
            _transactionStore = transactionStore;
        }
    }
}