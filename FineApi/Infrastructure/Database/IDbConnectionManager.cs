﻿using System.Data;

namespace FineApi.Infrastructure.Database
{
    public interface IDbConnectionManager
    {
        IDbConnection Connection { get; }
    }
}