﻿namespace FineApi.Infrastructure.Database
{
    public interface IDbConnectionBuilder
    {
        string BuildConnectionString();
    }
}