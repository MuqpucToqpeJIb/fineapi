﻿using System.Data;
using Npgsql;

namespace FineApi.Infrastructure.Database
{
    public class DbConnectionManager : IDbConnectionManager
    {
        private readonly IDbConnectionBuilder DbConnectionBuilder;

        public DbConnectionManager(IDbConnectionBuilder dbConnectionBuilder)
        {
            DbConnectionBuilder = dbConnectionBuilder;
        }

        public IDbConnection Connection => new NpgsqlConnection($"{DbConnectionBuilder.BuildConnectionString()};");
    }
}