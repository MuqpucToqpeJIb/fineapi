﻿using Microsoft.Extensions.Options;

namespace FineApi.Infrastructure.Database
{
    public class DbConnectionBuilder : IDbConnectionBuilder
    {
        private readonly IOptions<DbOptions> DbOptions;

        public DbConnectionBuilder(IOptions<DbOptions> dbOptions)
        {
            DbOptions = dbOptions;
        }

        public string BuildConnectionString()
        {
            var connectionString = $"UserID={DbOptions.Value.UserId};" +
                                   $"Password={DbOptions.Value.Password};" +
                                   $"Host={DbOptions.Value.Host};" +
                                   $"Port={DbOptions.Value.Port};" +
                                   $"Database={DbOptions.Value.Database};" +
                                   $"Pooling={DbOptions.Value.Pooling};" +
                                   $"SearchPath={DbOptions.Value.SearchPath}";

            return connectionString;
        }
    }
}