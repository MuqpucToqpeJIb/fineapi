﻿namespace FineApi.Infrastructure.Database
{
    public class DbOptions
    {
        public string UserId { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public string Database { get; set; }
        public string Pooling { get; set; }
        public string SearchPath { get; set; }
    }
}