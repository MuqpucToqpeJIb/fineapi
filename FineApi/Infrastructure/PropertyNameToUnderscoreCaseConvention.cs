﻿using Dapper.FluentMap.Conventions;
using FineApi.Extensions;

namespace FineApi.Infrastructure
{
    public class PropertyNameToUnderscoreCaseConvention : Convention
    {
        public PropertyNameToUnderscoreCaseConvention()
        {
            Properties().Configure(c => c.Transform(s => s.ToUnderscoreCase()));
        }
    }
}
