﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using FineApi.Infrastructure.Authorization;

namespace FineApi.Infrastructure.Middleware
{
    public class BasicAuthorizationMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly BasicAuthorizationOptions _options;

        public BasicAuthorizationMiddleware(RequestDelegate next, IOptions<BasicAuthorizationOptions> options)
        {
            _next = next;
            _options = options.Value;
        }

        public async Task Invoke(HttpContext context)
        {
            var serviceProvieder = context.RequestServices;
            //IBasicAuthorizationHandler handler = null;
            var route = _options.Handlers.FirstOrDefault(r => {
                var h = (IBasicAuthorizationHandler)serviceProvieder.GetService(r);
                return h.MustHandle(context.Request);
            });
            if (route == null)
            {
                await _next.Invoke(context);
                return;
            }
            var handler = (IBasicAuthorizationHandler)serviceProvieder.GetService(route);
            string authHeader = context.Request.Headers["Authorization"];
            if (authHeader != null && authHeader.StartsWith("Basic"))
            {
                //Extract credentials
                var encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim();
                var encoding = Encoding.UTF8;
                var usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

                var seperatorIndex = usernamePassword.IndexOf(':');
                var credentials = new BasicAuthorizationCredentials
                {
                    Login = usernamePassword.Substring(0, seperatorIndex),
                    Password = usernamePassword.Substring(seperatorIndex + 1)
                };

                //Here is the tricky bit
                if (handler.Authorize(credentials, out var user))
                {
                    context.User = user;
                    await _next.Invoke(context);
                    return;
                }
                handler.OnUnathorized(context);
            }
            else
            {
                handler.OnUnathorized(context);
            }
        }
    }
}