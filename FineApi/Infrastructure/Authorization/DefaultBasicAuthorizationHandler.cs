﻿using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace FineApi.Infrastructure.Authorization
{
    public abstract class DefaultBasicAuthorizationHandler
    {
        public abstract bool MustHandle(HttpRequest request);

        public abstract bool Authorize(BasicAuthorizationCredentials credentials, out ClaimsPrincipal principle);

        public void OnUnathorized(HttpContext context)
        {
            context.Response.StatusCode = 401;
        }
    }
}