﻿using System.Net;

namespace FineApi.Infrastructure.Authorization
{
    public class BasicAuthorizationErrorResponse<T>
    {
        public HttpStatusCode Code { get; set; }
        T Response { get; set; }
    }
}