﻿using System;
using System.Collections.Generic;

namespace FineApi.Infrastructure.Authorization
{
    public class BasicAuthorizationOptions
    {
        private readonly IList<Type> _handlers;
        public BasicAuthorizationOptions()
        {
            _handlers = new List<Type>();
        }
        public IEnumerable<Type> Handlers
        {
            get
            {
                return _handlers;
            }
        }
        public void AddHandler<T>() where T : IBasicAuthorizationHandler
        {
            var handlerType = typeof(T);
            _handlers.Add(handlerType);
        }
    }
}