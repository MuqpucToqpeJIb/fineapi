﻿using System.IO;
using System.Security.Claims;
using FineApi.JsonRpc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using FineApi.Entity.Enum;

namespace FineApi.Infrastructure.Authorization.Cashbox
{
    public class CashboxAuthorizationHandler : IBasicAuthorizationHandler
    {
        private readonly CashboxOptions _options;

        public CashboxAuthorizationHandler(IOptionsSnapshot<CashboxOptions> options)
        {
            _options = options.Value;
        }

        public bool MustHandle(HttpRequest request)
        {
            return request.Method == "POST" && request.Path.HasValue && (request.Path.Value == "/paycom");
        }

        public bool Authorize(BasicAuthorizationCredentials credentials, out ClaimsPrincipal principle)
        {
            principle = null;
            return credentials.Login == _options.Login && credentials.Password == _options.Key; // "UjBFnd4agi7QXrcEA&e2i#nQ8r#spkQzvrvA";
        }

        public void OnUnathorized(HttpContext context)
        {
            context.Response.StatusCode = 200;
            var id = GetRpcQueryId(context.Request.Body);
            var message = JToken.FromObject(CashboxErrorMessage.Unathorized());
            context.Response.WriteAsync(JsonConvert.SerializeObject(new RpcResponse(id, new RpcError((int)CashboxError.Unauthorized, message))));
        }

        private static object GetRpcQueryId(Stream stream)
        {
            string jsonString;
            if (stream == null)
            {
                jsonString = null;
            }
            else
            {
                using (var streamReader = new StreamReader(stream))
                {
                    jsonString = streamReader.ReadToEnd().Trim();
                }
            }
            var token = JToken.Parse(jsonString);
            var serializer = JsonSerializer.Create();
            var rpcRequest = token.ToObject<RpcRequest>(serializer);
            return rpcRequest.Id;
        }
    }
}