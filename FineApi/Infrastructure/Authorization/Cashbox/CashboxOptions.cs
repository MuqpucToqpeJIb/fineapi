﻿namespace FineApi.Infrastructure.Authorization.Cashbox
{
    public class CashboxOptions
    {
        public string Id { get; set; }
        public string Login { get; set; }
        public string Key { get; set; }
    }
}