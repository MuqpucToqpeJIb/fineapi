﻿namespace FineApi.Infrastructure.Authorization.Cashbox
{
    public class CashboxErrorMessage
    {
        public string ru { get; set; }
        public string uz { get; set; }
        public string en { get; set; }
        
        internal static CashboxErrorMessage NoPhoneFound()
        {
            return new CashboxErrorMessage
            {
                ru = "Номер телефона пользователя не найден",
                uz = "Foydalanuvchi telefon raqami topilmadi",
                en = "The phone number of the user was not found"
            };
        }
        internal static CashboxErrorMessage WrongSum()
        {
            return new CashboxErrorMessage
            {
                ru = "Сумма не может быть меньше 500 сум или более 1 млн. сум",
                uz = "To'lov 500 so'm kam bulmasligi va 1 million so'mdan ko'p bo'lmasligi lozim",
                en = "Sum must be more than 500 UZS and less than 1 million UZS"
            };
        }
        internal static CashboxErrorMessage Unathorized()
        {
            return new CashboxErrorMessage
            {
                ru = "Неавторизован для доступа",
                uz = "Avtorizatsiya xatosi",
                en = "Unauthorized"
            };
        }
        internal static CashboxErrorMessage PendingPayment()
        {
            return new CashboxErrorMessage
            {
                ru = "Невозможно создать новую транзакцию. Состояние счета: В ожидании оплаты",
                uz = "Yangi tranzaksiyani yaratib bo'lmaydi. Hisob balansi: to'lovni kutish",
                en = "Can not create a new transaction. Account balance: Pending payment"
            };
        }
        internal static CashboxErrorMessage NoTransactionId()
        {
            return new CashboxErrorMessage
            {
                ru = "ID транзакции не указана",
                uz = "Tranzaksiya IDsi ko'rsatilmagan",
                en = "No transaction ID provided"
            };
        }
        internal static CashboxErrorMessage TransactionStatusError()
        {
            return new CashboxErrorMessage
            {
                ru = "Ошибка статуса транзакции",
                uz = "Tranzaksiya statusi xato",
                en = "Wrong transaction status"
            };
        }
        internal static CashboxErrorMessage NoTransaction()
        {
            return new CashboxErrorMessage
            {
                ru = "Транзакции не найдена",
                uz = "Tranzaksiya mavjud emas",
                en = "No transaction found"
            };
        }
        internal static CashboxErrorMessage NotEnoughFundToRefund()
        {
            return new CashboxErrorMessage
            {
                ru = "Не достаточно средств для отмены транзакции",
                uz = "Tranzaksiya bekor qilish uchun hisobda yetarli mablag' mavjud emas",
                en = "Not enough funds on account to cancel transaction"
            };
        }
    }
}