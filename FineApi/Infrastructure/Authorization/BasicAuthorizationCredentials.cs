﻿namespace FineApi.Infrastructure.Authorization
{
    public class BasicAuthorizationCredentials
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}