﻿using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace FineApi.Infrastructure.Authorization
{
    public interface IBasicAuthorizationHandler
    {
        bool MustHandle(HttpRequest request);
        bool Authorize(BasicAuthorizationCredentials credentials, out ClaimsPrincipal principle);
        void OnUnathorized(HttpContext context);
    }
}