﻿namespace FineApi.Entity.Enum
{
    public enum TransactionStatus
    {
        CanceledAfterConfirm = -2,
        Canceled = -1,
        Pending = 1,
        Confirmed = 2
    }
}