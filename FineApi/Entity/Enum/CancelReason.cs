﻿namespace FineApi.Entity.Enum
{
    public enum CancelReason
    {
        /// <summary>
        /// Один или несколько получателей не найдены или не активны в Paycom.
        /// </summary>
        SomeRecieversNotFound = 1,
        /// <summary>
        /// Ошибка при выполнении дебетовой операции в процессингом центре.
        /// </summary>
        DebitOperationError = 2,
        /// <summary>
        /// Ошибка выполнения транзакции.
        /// </summary>
        TransactionperformError = 3,
        /// <summary>
        /// Транзакция отменена по таймауту.
        /// </summary>
        CanceledByTimeout = 4,
        /// <summary>
        /// Возврат денег.
        /// </summary>
        Refund = 5,
        /// <summary>
        /// Неизвестная ошибка.
        /// </summary>
        UnknownError = 10
    }
}