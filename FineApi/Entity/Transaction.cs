﻿using System;
using FineApi.Entity.Enum;

namespace FineApi.Entity
{
    public class Transaction
    {
        public long Id { get; set; }
        public long PeopleId { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? PerformTime { get; set; }
        public DateTime? CancelTime { get; set; }
        public int Amount { get; set; }
        public TransactionStatus Status { get; set; }
        public CancelReason? Reason { get; set; }
        public int ProviderId { get; set; }
    }
}