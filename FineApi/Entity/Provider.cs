﻿namespace FineApi.Entity
{
    public class Provider
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}