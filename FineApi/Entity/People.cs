﻿using System;
using FineApi.Entity.Enum;

namespace FineApi.Entity
{
    public class People
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public DateTime? Birthday { get; set; }
        public long? NationalityId { get; set; }
        public long? CitizenshipId { get; set; }
        public byte[] Photo { get; set; }
        public string AddressReal { get; set; }
        public string AddressLegal { get; set; }
        public string PhoneMobile { get; set; }
        public string PhoneHome { get; set; }
        public string Email { get; set; }
        public Gender Gender { get; set; }
        public bool PhoneHomeConfirmed { get; set; }
        public bool PhoneMobileConfirmed { get; set; }
        public bool EmailConfirmed { get; set; }
    }
}