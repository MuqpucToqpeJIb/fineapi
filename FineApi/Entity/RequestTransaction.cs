﻿using System;
using System.Data;
using FineApi.Entity.Enum;

namespace FineApi.Entity
{
    public class RequestTransaction
    {
        public string Id { get; set; }
        public long PeopleId { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? PerformTime { get; set; }
        public DateTime? CancelTime { get; set; }
        public DateTime ProviderCreateTime { get; set; }
        public DateTime? ProviderPerformTime { get; set; }
        public DateTime? ProviderCancelTime { get; set; }
        public int Amount { get; set; }
        public TransactionStatus Status { get; set; }
        public CancelReason? Reason { get; set; }
        public TransactionStatus ProviderStatus { get; set; }
        public CancelReason? ProviderReason { get; set; }
        public int ProviderId { get; set; }
        public long TransactionId { get; set; }
        public string Phone { get; set; }
    }

    public class RequestPaymentTransaction
    {
        public string Id { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int Amount { get; set; }
        public string Number { get; set; }
    }
}