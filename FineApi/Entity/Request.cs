﻿using System;
using FineApi.Entity.Enum;

namespace FineApi.Entity
{
    public class Request
    {
        public string Id { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? PerformTime { get; set; }
        public DateTime? CancelTime { get; set; }
        public TransactionStatus Status { get; set; }
        public CancelReason? Reason { get; set; }
        public long TransactionId { get; set; }
    }
}