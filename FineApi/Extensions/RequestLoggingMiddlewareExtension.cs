﻿using Microsoft.AspNetCore.Builder;
using FineApi.Infrastructure.Middleware;

namespace FineApi.Extensions
{
    public static class RequestLoggingMiddlewareExtension
    {
        public static IApplicationBuilder UseRequestLogging(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RequestLoggingMiddleware>();
        }
    }
}