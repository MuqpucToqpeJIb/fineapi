﻿using System;

namespace FineApi.Extensions
{
    public static class DateTimeExtension
    {
        public static readonly DateTime UnixStartTimestampDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static int ToUnixTime(this DateTime date)
        {
            return (int) date.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
        }

        public static long ToUnixTimeLong(this DateTime date)
        {
            return (long) date.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
        }

        public static DateTime ToDateTime(this long unixTimestamp)
        {
            return new DateTime(1970, 1, 1).AddMilliseconds(unixTimestamp);
        }
    }
}
