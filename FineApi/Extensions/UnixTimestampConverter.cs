﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace FineApi.Extensions
{
    public class UnixTimestampConverter : DateTimeConverterBase
    {
        public override bool CanWrite
        {
            get
            {
                var can = base.CanWrite;
                return can;
            }
        }

        public override bool CanConvert(Type objectType)
        {
            var can = base.CanConvert(objectType);
            return can;
        }


        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
                writer.WriteRawValue("0");
            else
                writer.WriteRawValue(((((DateTime)value).ToUnixTimeLong() + "")));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return ((long?)reader.Value)?.ToDateTime();
        }
    }
}