﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using FineApi.Infrastructure.Authorization;
using FineApi.Infrastructure.Middleware;

namespace FineApi.Extensions
{
    public static class BasicAuthExtension
    {
        public static void AddBasicAuth<T>(this IServiceCollection services) where T : class, IBasicAuthorizationHandler
        {
            services.AddTransient<T>();
            services.Configure<BasicAuthorizationOptions>(e =>
            {
                e.AddHandler<T>();
            });
        }
        public static void UseBasicAuth(this IApplicationBuilder app)
        {
            app.UseMiddleware<BasicAuthorizationMiddleware>();
        }
    }
}