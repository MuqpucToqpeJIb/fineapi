﻿using Microsoft.Extensions.Logging;
using FineApi.Infrastructure.Database;
using FineApi.Store.Interfaces;

namespace FineApi.Store
{
    public class TransactionStore : Store, ITransactionStore
    {
        public TransactionStore(IDbConnectionManager dbConnectionManager, ILoggerFactory loggerFactory) : base(dbConnectionManager, loggerFactory)
        {
        }
    }
}