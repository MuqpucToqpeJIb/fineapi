﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Logging;
using FineApi.Entity;
using FineApi.Entity.Enum;
using FineApi.Infrastructure.Database;
using FineApi.Store.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace FineApi.Store
{
    public class RequestTransactionStore : FineApi.Store.Store, IRequestTransactionStore
    {
        public RequestTransactionStore(IDbConnectionManager dbConnectionManager, ILoggerFactory loggerFactory) : base(dbConnectionManager, loggerFactory)
        {
        }

        /// <inheritdoc />
        /// <summary>
        /// Возвращает paycom запрос и транзакцию по идентификатору транзакции, провайдера
        /// </summary>
        /// <param name="id">Идентификатор транзакции, провайдера</param>
        /// <returns></returns>
        public async Task<RequestTransaction> GetById(string id)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                try
                {
                    return await dbConnection.QueryFirstOrDefaultAsync<RequestTransaction>(@"SELECT 
                                                                                            req.id, 
                                                                                            req.create_time AS provider_create_time, 
                                                                                            req.perform_time AS provider_perform_time, 
                                                                                            req.cancel_time AS provider_cancel_time, 
                                                                                            req.status AS provider_status, 
                                                                                            req.reason AS provider_reason, 
                                                                                            req.transaction_id,
                                                                                            tr.people_id, 
                                                                                            tr.create_time, 
                                                                                            tr.perform_time, 
                                                                                            tr.cancel_time, 
                                                                                            tr.amount, 
                                                                                            tr.status, 
                                                                                            tr.reason,
                                                                                            tr.provider_id
                                                                                            FROM paycom_requests AS req
                                                                                            JOIN transactions AS tr ON tr.id = req.transaction_id
                                                                                            WHERE req.id = @Id;", new { id });
                }
                catch (Exception e)
                {
                    Logger.LogError(e.GetBaseException().Message);
                    throw;
                }
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Создает paycom запрос и транзакцию
        /// </summary>
        /// <param name="id">Идентификатор транзакции, провайдера</param>
        /// <param name="peopleId">Идентификатор пользователя</param>
        /// <param name="createdAt">Время создания транзакции провайдером</param>
        /// <param name="amount">Сумма платежа</param>
        /// <returns></returns>
        public async Task<RequestTransaction> Create(string id, long peopleId, DateTime createdAt, int amount)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                using (var dbTransaction = dbConnection.BeginTransaction())
                {
                    try
                    {
                        await dbConnection.ExecuteAsync(@"SET CONSTRAINTS ALL DEFERRED;");
                        var transaction = new Transaction
                        {
                            PeopleId = peopleId,
                            CreateTime = DateTime.Now,
                            Amount = amount,
                            Status = TransactionStatus.Pending,
                            ProviderId = (int)Entity.Enum.Provider.Paycom
                        };
                        transaction.Id = await dbConnection.QueryFirstOrDefaultAsync<long>(@"INSERT INTO transactions(people_id, create_time, amount, status, provider_id)
                                                                                            VALUES (@PeopleId, @CreateTime, @Amount, @Status, @ProviderId)
                                                                                            RETURNING id;",
                            new { transaction.PeopleId, transaction.CreateTime, transaction.Amount, transaction.Status, transaction.ProviderId });
                        var request = new Request
                        {
                            Id = id,
                            CreateTime = createdAt,
                            Status = TransactionStatus.Pending,
                            TransactionId = transaction.Id
                        };
                        await dbConnection.ExecuteAsync(@"INSERT INTO paycom_requests(id, create_time, status, transaction_id)
                                                        VALUES (@Id, @CreateTime, @Status, @TransactionId);",
                            new
                            {
                                request.Id,
                                request.CreateTime,
                                request.Status,
                                request.TransactionId
                            });
                        await dbConnection.ExecuteAsync(@"SET CONSTRAINTS ALL IMMEDIATE;");
                        dbTransaction.Commit();
                        var requestTransaction = new RequestTransaction
                        {
                            Id = request.Id,
                            PeopleId = transaction.PeopleId,
                            CreateTime = transaction.CreateTime,
                            ProviderCreateTime = request.CreateTime,
                            Amount = transaction.Amount,
                            Status = transaction.Status,
                            ProviderStatus = request.Status,
                            ProviderId = transaction.ProviderId,
                            TransactionId = transaction.Id

                        };
                        return requestTransaction;
                    }
                    catch (Exception e)
                    {
                        Logger.LogError(e.GetBaseException().Message);
                        if (dbTransaction != null)
                        {
                            dbTransaction.Rollback();
                            dbTransaction.Dispose();
                        }
                        throw;
                    }
                    finally
                    {
                        dbTransaction?.Dispose();
                    }
                }
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Отмена paycom запроса и транзакции
        /// </summary>
        /// <param name="id">Идентификатор транзакции, провайдера</param>
        /// <param name="reason">Причина отмены транзакции</param>
        /// <returns></returns>
        public async Task<RequestTransaction> Cancel(string id, CancelReason reason)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                var dbTransaction = dbConnection.BeginTransaction();
                try
                {
                    var request = await dbConnection.QueryFirstOrDefaultAsync<Request>(@"UPDATE paycom_requests
                                                                                       SET cancel_time = @CancelTime, status = @Status, reason = @Reason
                                                                                       WHERE id = @Id
                                                                                       RETURNING *;",
                        new
                        {
                            CancelTime = DateTime.UtcNow,
                            Status = TransactionStatus.Canceled,
                            Reason = (int)reason,
                            Id = id
                        });

                    var transaction = await dbConnection.QueryFirstOrDefaultAsync<Transaction>(@"UPDATE transactions
                                                                                               SET cancel_time = @CancelTime, status = @Status, reason = @Reason
                                                                                               WHERE id = @Id
                                                                                               RETURNING *;",
                        new
                        {
                            CancelTime = DateTime.Now,
                            Status = TransactionStatus.Canceled,
                            Reason = (int)reason,
                            Id = request.TransactionId
                        });
                    dbTransaction.Commit();
                    var requestTransaction = new RequestTransaction
                    {
                        Id = request.Id,
                        PeopleId = transaction.PeopleId,
                        CreateTime = transaction.CreateTime,
                        ProviderCreateTime = request.CreateTime,
                        PerformTime = transaction.PerformTime,
                        ProviderPerformTime = request.PerformTime,
                        CancelTime = transaction.CancelTime,
                        ProviderCancelTime = request.CancelTime,
                        Amount = transaction.Amount,
                        Status = transaction.Status,
                        ProviderStatus = request.Status,
                        Reason = transaction.Reason,
                        ProviderReason = request.Reason,
                        ProviderId = transaction.ProviderId,
                        TransactionId = transaction.Id
                    };
                    return requestTransaction;
                }
                catch (Exception e)
                {
                    Logger.LogError(e.GetBaseException().Message);
                    throw;
                }
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Подтверждение paycom запроса и транзакции
        /// </summary>
        /// <param name="id">Идентификатор транзакции, провайдера</param>
        /// <returns></returns>
        public async Task<RequestTransaction> Perform(string id)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                var dbTransaction = dbConnection.BeginTransaction();
                try
                {
                    var request = await dbConnection.QueryFirstOrDefaultAsync<Request>(@"UPDATE paycom_requests
                                                                                       SET perform_time = @PerformTime, status = @Status, reason = @Reason
                                                                                       WHERE id = @Id
                                                                                       RETURNING *;",
                        new
                        {
                            PerformTime = DateTime.UtcNow,
                            Status = TransactionStatus.Confirmed,
                            Id = id
                        });
                    var transaction = await dbConnection.QueryFirstOrDefaultAsync<Transaction>(@"UPDATE transactions
                                                                                               SET perform_time = @PerformTime, status = @Status, reason = @Reason
                                                                                               WHERE id = @Id
                                                                                               RETURNING *;",
                        new
                        {
                            PerformTime = DateTime.Now,
                            Status = TransactionStatus.Confirmed,
                            Id = request.TransactionId
                        });
                    await dbConnection.QueryFirstOrDefaultAsync<People>(@"UPDATE people
                                                                          SET balance = balance + @Amount
                                                                          WHERE id = @PeopleId;", new
                    {
                        transaction.PeopleId,
                        transaction.Amount
                    });
                    dbTransaction.Commit();
                    var requestTransaction = new RequestTransaction
                    {
                        Id = request.Id,
                        PeopleId = transaction.PeopleId,
                        CreateTime = transaction.CreateTime,
                        ProviderCreateTime = request.CreateTime,
                        PerformTime = transaction.PerformTime,
                        ProviderPerformTime = request.PerformTime,
                        CancelTime = transaction.CancelTime,
                        ProviderCancelTime = request.CancelTime,
                        Amount = transaction.Amount,
                        Status = transaction.Status,
                        ProviderStatus = request.Status,
                        Reason = transaction.Reason,
                        ProviderReason = request.Reason,
                        ProviderId = transaction.ProviderId,
                        TransactionId = transaction.Id
                    };
                    return requestTransaction;
                }
                catch (Exception e)
                {
                    Logger.LogError(e.GetBaseException().Message);
                    throw;
                }
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Отмена paycom запроса и транзакции после подтверждения
        /// </summary>
        /// <param name="id">Идентификатор транзакции, провайдера</param>
        /// <param name="reason">Причина отмены транзакции</param>
        /// <returns></returns>
        public async Task<RequestTransaction> CancelAfterConfirm(string id, CancelReason reason)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                var dbTransaction = dbConnection.BeginTransaction();
                try
                {
                    var request = await dbConnection.QueryFirstOrDefaultAsync<Request>(@"UPDATE paycom_requests
                                                                                       SET cancel_time = @CancelTime, status = @Status, reason = @Reason
                                                                                       WHERE id = @Id
                                                                                       RETURNING *;",
                        new
                        {
                            CancelTime = DateTime.UtcNow,
                            Status = TransactionStatus.CanceledAfterConfirm,
                            Reason = reason,
                            Id = id
                        });
                    var transaction = await dbConnection.QueryFirstOrDefaultAsync<Transaction>(@"UPDATE transactions
                                                                                               SET cancel_time = @CancelTime, status = @Status, reason = @Reason
                                                                                               WHERE id = @Id
                                                                                               RETURNING *;",
                        new
                        {
                            CancelTime = DateTime.Now,
                            Status = TransactionStatus.CanceledAfterConfirm,
                            Reason = reason,
                            Id = request.TransactionId
                        });
                    await dbConnection.QueryFirstOrDefaultAsync<People>(@"UPDATE people
                                                                          SET balance = balance - @Amount
                                                                          WHERE id = @PeopleId;", new
                    {
                        transaction.PeopleId,
                        transaction.Amount
                    });
                    dbTransaction.Commit();
                    var requestTransaction = new RequestTransaction
                    {
                        Id = request.Id,
                        PeopleId = transaction.PeopleId,
                        CreateTime = transaction.CreateTime,
                        ProviderCreateTime = request.CreateTime,
                        PerformTime = transaction.PerformTime,
                        ProviderPerformTime = request.PerformTime,
                        CancelTime = transaction.CancelTime,
                        ProviderCancelTime = request.CancelTime,
                        Amount = transaction.Amount,
                        Status = transaction.Status,
                        ProviderStatus = request.Status,
                        Reason = transaction.Reason,
                        ProviderReason = request.Reason,
                        ProviderId = transaction.ProviderId,
                        TransactionId = transaction.Id
                    };
                    return requestTransaction;
                }
                catch (Exception e)
                {
                    Logger.LogError(e.GetBaseException().Message);
                    throw;
                }
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Возвращает список paycom запросов и транзакций за период
        /// </summary>
        /// <param name="dateFrom">Дата начала периода</param>
        /// <param name="dateTo">Дата окончания периода</param>
        /// <returns></returns>
        public async Task<IEnumerable<RequestTransaction>> GetByPeriod(DateTime dateFrom, DateTime dateTo)
        {
            {
                using (var dbConnection = Connection)
                {
                    dbConnection.Open();
                    try
                    {
                        return await dbConnection.QueryAsync<RequestTransaction>(@"SELECT 
                                                                                req.id, 
                                                                                req.create_time AS provider_create_time, 
                                                                                req.perform_time AS provider_perform_time, 
                                                                                req.cancel_time AS provider_cancel_time, 
                                                                                req.status AS provider_status, 
                                                                                req.reason AS provider_reason, 
                                                                                req.transaction_id,
                                                                                tr.people_id, 
                                                                                tr.create_time, 
                                                                                tr.perform_time, 
                                                                                tr.cancel_time, 
                                                                                tr.amount, 
                                                                                tr.status, 
                                                                                tr.reason,
                                                                                tr.provider_id
                                                                                FROM paycom_requests AS req
                                                                                JOIN transactions AS tr ON tr.id = req.transaction_id
                                                                                WHERE tr.create_time >= @dateFrom::date AND tr.create_time <= @dateTo::date;",
                                                                                new { DateFrom = dateFrom, DateTo = dateTo });
                    }
                    catch (Exception e)
                    {
                        Logger.LogError(e.GetBaseException().Message);
                        throw;
                    }
                }
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Возвращает список список paycom запросов и транзакций пользователя по статусу
        /// </summary>
        /// <param name="peopleId">Идентификатор пользователя</param>
        /// <param name="status">Статус транзакции</param>
        /// <returns></returns>
        public async Task<IEnumerable<RequestTransaction>> GetByPeopleIdAndStatus(long peopleId, TransactionStatus status)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                try
                {
                    return await dbConnection.QueryAsync<RequestTransaction>(@"SELECT 
                                                                            req.id, 
                                                                            req.create_time AS provider_create_time, 
                                                                            req.perform_time AS provider_perform_time, 
                                                                            req.cancel_time AS provider_cancel_time, 
                                                                            req.status AS provider_status, 
                                                                            req.reason AS provider_reason, 
                                                                            req.transaction_id,
                                                                            tr.people_id, 
                                                                            tr.create_time, 
                                                                            tr.perform_time, 
                                                                            tr.cancel_time, 
                                                                            tr.amount, 
                                                                            tr.status, 
                                                                            tr.reason,
                                                                            tr.provider_id, 
                                                                            pp.phone_mobile
                                                                            FROM paycom_requests AS req
                                                                            JOIN transactions AS tr ON tr.id = req.transaction_id
                                                                            JOIN people AS pp ON pp.id = tr.people_id
                                                                            WHERE tr.people_id = @PeopleId AND tr.status = @Status;", new { PeopleId = peopleId, Status = status });
                }
                catch (Exception e)
                {
                    Logger.LogError(e.GetBaseException().Message);
                    throw;
                }
            }
        }
    }
}