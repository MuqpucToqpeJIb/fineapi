﻿using Microsoft.Extensions.Logging;
using FineApi.Infrastructure.Database;
using FineApi.Store.Interfaces;

namespace FineApi.Store
{
    public class RequestStore : Store, IRequestStore
    {
        public RequestStore(IDbConnectionManager dbConnectionManager, ILoggerFactory loggerFactory) : base(dbConnectionManager, loggerFactory)
        {
        }
    }
}