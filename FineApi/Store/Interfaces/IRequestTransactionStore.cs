﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FineApi.Entity;
using FineApi.Entity.Enum;

namespace FineApi.Store.Interfaces
{
    public interface IRequestTransactionStore
    {
        /// <summary>
        /// Возвращает paycom запрос и транзакцию по идентификатору транзакции, провайдера
        /// </summary>
        /// <param name="id">Идентификатор транзакции, провайдера</param>
        /// <returns></returns>
        Task<RequestTransaction> GetById(string id);
        /// <summary>
        /// Создает paycom запрос и транзакцию
        /// </summary>
        /// <param name="id">Идентификатор транзакции, провайдера</param>
        /// <param name="peopleId">Идентификатор пользователя</param>
        /// <param name="createdAt">Время создания транзакции провайдером</param>
        /// <param name="amount">Сумма платежа</param>
        /// <returns></returns>
        Task<RequestTransaction> Create(string id, long peopleId, DateTime createdAt, int amount);
        /// <summary>
        /// Отмена paycom запроса и транзакции
        /// </summary>
        /// <param name="id">Идентификатор транзакции, провайдера</param>
        /// <param name="reason">Причина отмены транзакции</param>
        /// <returns></returns>
        Task<RequestTransaction> Cancel(string id, CancelReason reason);
        /// <summary>
        /// Подтверждение paycom запроса и транзакции
        /// </summary>
        /// <param name="id">Идентификатор транзакции, провайдера</param>
        /// <returns></returns>
        Task<RequestTransaction> Perform(string id);
        /// <summary>
        /// Отмена paycom запроса и транзакции после подтверждения
        /// </summary>
        /// <param name="id">Идентификатор транзакции, провайдера</param>
        /// <param name="reason">Причина отмены транзакции</param>
        /// <returns></returns>
        Task<RequestTransaction> CancelAfterConfirm(string id, CancelReason reason);
        /// <summary>
        /// Возвращает список paycom запросов и транзакций за период
        /// </summary>
        /// <param name="dateFrom">Дата начала периода</param>
        /// <param name="dateTo">Дата окончания периода</param>
        /// <returns></returns>
        Task<IEnumerable<RequestTransaction>> GetByPeriod(DateTime dateFrom, DateTime dateTo);
        /// <summary>
        /// Возвращает список список paycom запросов и транзакций пользователя по статусу
        /// </summary>
        /// <param name="peopleId">Идентификатор пользователя</param>
        /// <param name="status">Статус транзакции</param>
        /// <returns></returns>
        Task<IEnumerable<RequestTransaction>> GetByPeopleIdAndStatus(long peopleId, TransactionStatus status);
    }
}