﻿using System.Threading.Tasks;
using FineApi.Entity;

namespace FineApi.Store.Interfaces
{
    public interface IPeopleStore
    {
        /// <summary>
        /// Возвращает пользователя по его номеру мобильного телефона
        /// </summary>
        /// <param name="phoneMobile">Номер мобильного телефона</param>
        /// <returns></returns>
        Task<People> GetByPhoneMobile(string phoneMobile);
    }
}