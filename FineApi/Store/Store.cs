﻿using System.Data;
using Microsoft.Extensions.Logging;
using FineApi.Infrastructure.Database;
using FineApi.Store.Interfaces;

namespace FineApi.Store
{
    public class Store : IStore
    {
        public readonly IDbConnectionManager DbConnectionManager;
        public readonly ILoggerFactory LoggerFactory;
        public readonly ILogger Logger;

        public Store(IDbConnectionManager dbConnectionManager, ILoggerFactory loggerFactory)
        {
            DbConnectionManager = dbConnectionManager;
            LoggerFactory = loggerFactory;
            Logger = loggerFactory.CreateLogger("Store");
        }

        public IDbConnection Connection => DbConnectionManager.Connection;
    }
}
