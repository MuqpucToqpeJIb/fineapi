﻿using System;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Logging;
using FineApi.Entity;
using FineApi.Infrastructure.Database;
using FineApi.Store.Interfaces;

namespace FineApi.Store
{
    public class PeopleStore : Store, IPeopleStore
    {
        public PeopleStore(IDbConnectionManager dbConnectionManager, ILoggerFactory loggerFactory) : base(dbConnectionManager, loggerFactory)
        {
        }

        /// <inheritdoc />
        /// <summary>
        /// Возвращает пользователя по его номеру мобильного телефона
        /// </summary>
        /// <param name="phoneMobile">Номер мобильного телефона</param>
        /// <returns></returns>
        public async Task<People> GetByPhoneMobile(string phoneMobile)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                try
                {
                    return await dbConnection.QueryFirstOrDefaultAsync<People>(@"SELECT * FROM people 
                                                                                WHERE phone_mobile = @PhoneMobile AND phone_mobile_confirmed = TRUE
                                                                                LIMIT 1", new { PhoneMobile = phoneMobile });
                }
                catch (Exception e)
                {
                    Logger.LogError(e.GetBaseException().Message);
                    throw;
                }
            }
        }
    }
}