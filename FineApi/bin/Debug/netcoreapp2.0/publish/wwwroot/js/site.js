﻿$(document).ready(function () {
   $('.info').css('display','none');
    $('.nums').click(function () {
        var id = $(this).attr('id');
       $.get({
           url: "/api/Fines/"+id,
           success: function (data) {
               var date = data.date.split("T");
               var days = date[0];
               var time = date[1];
               
               var times = time.split(":");
               var dateTime = new Date(days.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
               console.log(dateTime);
               $('.info').css('display','block');
               $('.date').text(dateTime.getDate() +"/"+ (dateTime.getMonth()+1)+"/"+dateTime.getFullYear()+ " "+ times[0]+ ":" + times[1]);
               $('.number').text(data.registrationNumber);
               $('.payment').text(data.payment);
               $('.reason').text(data.reason);
               $('.area').text(data.area);
               $('.parking').text(data.parking);
               $('.worker').text(data.worker);
               if (data.photo){
               $('.img-photo').attr('src', data.photo.src);
               }else{
                   $('.img-photo').attr('src', "")
               }
               initMap(data.location.latitude, data.location.longitude)
           }
       }); 
    });
    
    
    
});
function initMap(lat, lng) {
    var uluru = {lat: parseInt(lat), lng: parseInt(lng)};
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 14,
        center: uluru
    });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map
    });
}