﻿namespace FineApi.Models
{
    public class ChangePasswordResult
    {
        public bool Success { get; set; }
    }
}