﻿using System.Collections.Generic;

namespace FineApi.Models
{
    public class GetStatementResult<T>
    {
        public IEnumerable<TransactionModel<T>> Transactions { get; set; }
    }
}