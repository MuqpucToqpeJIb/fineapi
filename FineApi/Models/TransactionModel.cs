﻿using System;
using Newtonsoft.Json;
using FineApi.Entity.Enum;
using FineApi.Extensions;

namespace FineApi.Models
{
    public class TransactionModel<T>
    {
        public string Id { get; set; }
        [JsonConverter(typeof(UnixTimestampConverter))]
        public DateTime Time { get; set; }
        public int Amount { get; set; }
        public T Account { get; set; }
        [JsonConverter(typeof(UnixTimestampConverter))]
        public DateTime CreateTime { get; set; }
        [JsonConverter(typeof(UnixTimestampConverter))]
        public DateTime PerformTime { get; set; }
        [JsonConverter(typeof(UnixTimestampConverter))]
        public DateTime CancelTime { get; set; }
        public string Transaction { get; set; }
        public TransactionStatus State { get; set; }
        public CancelReason Reason { get; set; }
        public ReceiverModel[] Receivers { get; set; }
    }
}