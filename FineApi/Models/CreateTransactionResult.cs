﻿using System;
using Newtonsoft.Json;
using FineApi.Entity.Enum;
using FineApi.Extensions;

namespace FineApi.Models
{
    public class CreateTransactionResult
    {
        [JsonConverter(typeof(UnixTimestampConverter))]
        public DateTime CreateTime { get; set; }
        public string Transaction { get; set; }
        public TransactionStatus State { get; set; }
    }
}