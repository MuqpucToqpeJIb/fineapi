﻿using System;
using Newtonsoft.Json;
using FineApi.Extensions;

namespace FineApi.Models
{
    public class PerformTransactionResult
    {
        public string Transaction { get; set; }
        [JsonConverter(typeof(UnixTimestampConverter))]
        public DateTime PerformTime { get; set; }
        public int State { get; set; }
    }
}