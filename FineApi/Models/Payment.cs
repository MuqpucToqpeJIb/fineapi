﻿using System;
using FineApi.Entity.Enum;
using FineApi.Extensions;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Newtonsoft.Json;

namespace FineApi.Models
{
    public class Payment
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public DateTime Start { get; set; } = DateTime.Now.AddHours(+8);
        public int Amount { get; set; }
        public TransactionStatus Status { get; set; } = TransactionStatus.Pending;
        public DateTime End { get; set; }
        public string Transaction { get; set; }
        public DateTime? PerformTime { get; set; }
        public CancelReason? Reason { get; set; } = null;
        public DateTime? CancelTime { get; set; }
        public DateTime CreateTime { get; set; } = DateTime.Now.ToUniversalTime();

    }

}