﻿using System;
using FineApi.Entity.Enum;

namespace FineApi.Models
{
    public class CheckParkingModel
    {
        public string Result { get; set; }
        public ParkingInfo ParkingInfo { get; set; }
    }

    public class ParkingInfo
    {
        public string Number { get; set; }
        public DateTime StartParking { get; set; }
        public DateTime EndParking { get; set; }
        public int TransactionStatus { get; set; }
}
    
}