﻿using System;
using Newtonsoft.Json;
using FineApi.Extensions;

namespace FineApi.Models
{
    public class CancelTransactionResult
    {
        public string Transaction { get; set; }
        public int State { get; set; }

        [JsonConverter(typeof(UnixTimestampConverter))]
        public DateTime CancelTime { get; set; }
    }
}