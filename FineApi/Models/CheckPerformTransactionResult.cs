﻿namespace FineApi.Models
{
    public class CheckPerformTransactionResult
    {
        public bool Allow { get; set; }
    }
}