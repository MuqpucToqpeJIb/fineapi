﻿namespace FineApi.Models
{
    public class ReceiverModel
    {
        public string Id { get; set; }
        public int Amount { get; set; }
    }
}