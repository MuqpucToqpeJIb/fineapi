﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Scaffolding.Metadata;

namespace FineApi.Models
{
    public class Fine
    {
        [Key]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Payment { get; set; }
        public string Reason { get; set; }
        public string Worker { get; set; }
        public string RegistrationNumber { get; set; }
        public string Area { get; set; }
        public string Parking { get; set; }
        public Photo Photo { get; set; }
        public Location Location { get; set; }
    }

    public class Photo
    {
        public int Id { get; set; }
        public int FineId { get; set; }
        public string Src { get; set; }
        public Fine Fine { get; set; }
    }

    public class Location
    {
        public int Id { get; set; }
        public int FineId { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public Fine Fine { get; set; }
    }

    public class FineResult
    {
        private static int hor = 14;
        public Fine Fine { get; set; }
        public string Message { get; set; }
        public DateTime TimeServer { get; set; } = DateTime.Now;
        public DateTime TimePlas { get; set; } = DateTime.Now.AddHours(+8);
        public DateTime TimePlasHor { get; set; } = DateTime.Now.AddHours(hor+8);
    }
    internal class FineConfiguration : IEntityTypeConfiguration<Fine>
    {
        public void Configure(EntityTypeBuilder<Fine> builder)
        {
            builder.HasOne(x => x.Photo)
                .WithOne(x => x.Fine)
                .HasForeignKey<Photo>(x=> x.FineId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
            builder.HasOne(x => x.Location)
                .WithOne(x => x.Fine)
                .HasForeignKey<Location>(x=> x.FineId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}