﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FineApi.Models
{
    public class FineApiContext: DbContext
    {
        public FineApiContext(DbContextOptions<FineApiContext> options): base(options)
        {
         
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new FineConfiguration());
        }
        
        public DbSet<Fine> Fines { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<Payment> Payments { get; set; }
    }
}