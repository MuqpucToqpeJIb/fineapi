﻿using System;
using Newtonsoft.Json;
using FineApi.Entity.Enum;
using FineApi.Extensions;

namespace FineApi.Models
{
    public class CheckTransactionResult
    {
        [JsonConverter(typeof(UnixTimestampConverter))]
        public DateTime CreateTime { get; set; }
        [JsonConverter(typeof(UnixTimestampConverter))]
        public DateTime PerformTime { get; set; }
        [JsonConverter(typeof(UnixTimestampConverter))]
        public DateTime CancelTime { get; set; }
        public string Transaction { get; set; }
        public int State { get; set; }
        public CancelReason? Reason { get; set; }
    }
}