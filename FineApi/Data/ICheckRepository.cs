﻿using System.Threading.Tasks;
using FineApi.Entity;
using FineApi.Entity.Enum;
using FineApi.Models;

namespace FineApi.Data
{
    public interface ICheckRepository
    {
        CheckParkingModel CheckParking(string number);

    }
}