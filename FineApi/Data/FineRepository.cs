﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using FineApi.Models;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
 

namespace FineApi.Data
{
    public class FineRepository: IFineRepository
    {
        private readonly FineApiContext _ctx;

        public FineRepository(FineApiContext context, IUploadRepository upload)
        {
            this._ctx = context;
           
        }

        public IEnumerable<Fine> FetchFines()
        {
            return _ctx.Fines;
        }

        public Fine FetchFine(int id)
        {
            return _ctx.Fines.Include(x => x.Photo).Include(x => x.Location).SingleOrDefault(x => x.Id == id);
        }

        public async Task<FineResult> CreateFine(Fine fine)
        {
            FineResult result = new FineResult();
            if (fine == null)
            {
                result.Fine = null;
                result.Message = "Fine can not be null";
                return result;
            }
            
            _ctx.Fines.Add(fine);
            await _ctx.SaveChangesAsync();
            
            result.Fine = fine;
            result.Message = "Success";
            return result;

        }

        public async Task<string> UpdatePhoto(int id, string src)
        {
            
            var entity = _ctx.Photos.SingleOrDefault(x => x.FineId == id);
            if (entity == null)
            {
                _ctx.Photos.Add(new Photo() {FineId = id, Src = src});
                await _ctx.SaveChangesAsync();
                return "Success: " + src;
            }
            entity.Src = src;
            await _ctx.SaveChangesAsync();
            return "Succes: " + entity.Src;
        }
    }
}