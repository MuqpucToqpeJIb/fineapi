﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace FineApi.Data
{
    public class UploadRepository : IUploadRepository
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        public UploadRepository(IHostingEnvironment environment)
        {
            _hostingEnvironment = environment;
        }

        public async Task<string> Upload(List<IFormFile> files, string num)
        {
            var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "files", num);
            foreach (var file in files)
            {
                if (file.Length > 0)
                {
                    System.IO.Directory.CreateDirectory(uploads);
                    var filePath = Path.Combine(uploads, file.FileName);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                    }
                    uploads = file.FileName;
                }
            }
            return "/files/"+ num + "/" + uploads;
        }
    }
}