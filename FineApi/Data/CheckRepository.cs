﻿using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using FineApi.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication;

namespace FineApi.Data
{
    public class CheckRepository: ICheckRepository
    {
        private readonly FineApiContext _ctx;

        public CheckRepository(FineApiContext ctx)
        {
            _ctx = ctx; 
        }

        public CheckParkingModel CheckParking(string number)
        {
            var result = _ctx.Payments.OrderByDescending(x=> x.Start).FirstOrDefault(x=> x.Number == number && x.Start > DateTime.Now.AddHours(-24));
            
            if (result == null)
            {
                return new CheckParkingModel
                {
                    Result = "Unpaid",
                    ParkingInfo = null
                };
            }
            if (result.End < DateTime.Now)
            {
                return new CheckParkingModel
                {
                    Result = "Overdue",
                    ParkingInfo = new ParkingInfo
                    {
                         StartParking   = result.Start,
                         EndParking = result.End,
                         Number =  result.Number
                    }
                };
            }
           
            return new CheckParkingModel
            {
                Result = "Success",
                ParkingInfo =  new ParkingInfo
                {
                    Number = result.Number,
                    EndParking = result.End,
                    StartParking = result.Start
                }
            };
        }
    }
}