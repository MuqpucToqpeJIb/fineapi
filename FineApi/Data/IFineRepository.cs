﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FineApi.Models;

namespace FineApi.Data
{
    public interface IFineRepository
    {
        IEnumerable<Fine> FetchFines();
        Fine FetchFine(int id);
        Task<FineResult> CreateFine(Fine fine);
        Task<string> UpdatePhoto(int id, string src);
    }
}