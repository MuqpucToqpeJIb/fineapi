﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FineApi.Entity;
using FineApi.Entity.Enum;
using FineApi.Models;
using Microsoft.EntityFrameworkCore;

namespace FineApi.Data
{
    public class PaymentRepository: IPaymentRepository
    {
        private readonly FineApiContext _context;
        public PaymentRepository(FineApiContext context)
        {
            _context = context;
        }

        public async Task<Payment> CreatePayment(Payment payment)
        {
            var hours = payment.Amount / 100000;
            payment.End = payment.Start.AddHours(hours);
            payment.CreateTime = DateTime.UtcNow;
            _context.Payments.Add(payment);
            await _context.SaveChangesAsync();
            
            return  payment;
        }

       public Task<Payment> GetById(string id)
        {
            var result =  _context.Payments.FirstOrDefaultAsync(x => x.Transaction == id);
            return result;
        }
        
        public async Task<Payment> Perform(string id)
        {
            var entity = _context.Payments.FirstOrDefault(x => x.Transaction == id);
            entity.Status = TransactionStatus.Confirmed;
            entity.PerformTime = DateTime.UtcNow;
            await _context.SaveChangesAsync();
            return entity;
        }
        public async Task<Payment> Cancel(string id, CancelReason reason)
        {
            var entity = _context.Payments.FirstOrDefault(x => x.Transaction == id);
            if (entity.Status == TransactionStatus.Pending)
            {
                entity.Status = TransactionStatus.Canceled;
            }else if (entity.Status == TransactionStatus.Confirmed)
            {
                entity.Status = TransactionStatus.CanceledAfterConfirm;
            }
            
            entity.CancelTime = DateTime.UtcNow;
            entity.Reason = reason;
           // entity.PerformTime = DateTime.Now;
            await _context.SaveChangesAsync();
            return entity;
        }

        public IEnumerable<Payment> GetByPeriod(long from, long to)
        {
            return _context.Payments.Where(x => x.CreateTime > new DateTime(to) && x.CreateTime < new DateTime(from));
        }
    }
}