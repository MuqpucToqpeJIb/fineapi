﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace FineApi.Data
{
    public interface IUploadRepository
    {
        Task<string> Upload(List<IFormFile> files, string num);
    }
}