﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FineApi.Entity;
using FineApi.Entity.Enum;
using FineApi.Models;

namespace FineApi.Data
{
    public interface IPaymentRepository
    {
        Task<Payment> CreatePayment(Payment payment);
        Task<Payment> GetById(string id);
        Task<Payment> Perform(string id);
        Task<Payment> Cancel(string id, CancelReason reason);
        IEnumerable<Payment> GetByPeriod(long from, long to);
    }
}