﻿using System.IO;
using Dapper.FluentMap;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using NGC.SmartEdu.Paycom.Entity;
using NGC.SmartEdu.Paycom.Extensions;
using NGC.SmartEdu.Paycom.Infrastructure;
using NGC.SmartEdu.Paycom.Infrastructure.Authorization.Cashbox;
using NGC.SmartEdu.Paycom.Infrastructure.Database;
using NGC.SmartEdu.Paycom.Manager;
using NGC.SmartEdu.Paycom.Manager.Interfaces;
using NGC.SmartEdu.Paycom.Store;
using NGC.SmartEdu.Paycom.Store.Interfaces;
using Swashbuckle.AspNetCore.Swagger;

namespace NGC.SmartEdu.Paycom
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options =>
                {})
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    options.SerializerSettings.Converters.Add(new StringEnumConverter());
                    options.SerializerSettings.Converters.Add(new IsoDateTimeConverter
                    {
                        DateTimeFormat = "dd'-'MM'-'yyyy'T'HH':'mm':'ss.fff'Z'"
                    });
                });

            // Inject an implementation of ISwaggerProvider with defaulted settings applied
            services.AddSwaggerGen(options =>
            {
                options.IgnoreObsoleteActions();
                options.IgnoreObsoleteProperties();
                options.SwaggerDoc("v1", new Info
                {
                    Title = "Smart Education Paycom API",
                    Version = "1.0",
                    TermsOfService = "None",
                    Description = "",
                    Contact = new Contact { Name = "Mobis", Email = "", Url = "http://mss.uz/" },
                    License = new License()
                });

                //Determine base path for the application.
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;

                //Set the comments path for the swagger json and ui.
                var xmlPath = Path.Combine(basePath, "NGC.SmartEdu.Paycom.xml");
                options.IncludeXmlComments(xmlPath);
            });

            services.AddJsonRpc(config =>
            {
                //returns detailed error messages from server to rpcresponses
                config.ShowServerExceptions = true;
                config.JsonSerializerSettings = new JsonSerializerSettings()
                {
                    ContractResolver = new DefaultContractResolver()
                    {
                        NamingStrategy = new SnakeCaseNamingStrategy()
                    }
                };
            });

            services.AddApiVersioning(options =>
            {
                options.ApiVersionReader = new HeaderApiVersionReader("api-version");
            });

            services.Configure<DbOptions>(options =>
            {
                options.UserId = Configuration.GetSection("DbOptions:UserId").Value;
                options.Password = Configuration.GetSection("DbOptions:Password").Value;
                options.Host = Configuration.GetSection("DbOptions:Host").Value;
                options.Port = Configuration.GetSection("DbOptions:Port").Value;
                options.Database = Configuration.GetSection("DbOptions:Database").Value;
                options.Pooling = Configuration.GetSection("DbOptions:Pooling").Value;
                options.SearchPath = Configuration.GetSection("DbOptions:SearchPath").Value;
            });
            services.Configure<CashboxOptions>(e =>
            {
                e.Id = Configuration.GetSection("CashboxOptions:Id").Value;
                e.Login = Configuration.GetSection("CashboxOptions:Login").Value;
                e.Key = Configuration.GetSection("CashboxOptions:Key").Value;
            });

            services.AddBasicAuth<CashboxAuthorizationHandler>();
            
            // Initialization FluentMapper
            FluentMapper.Initialize(c =>
            {
                c.AddConvention<PropertyNameToUnderscoreCaseConvention>()
                .ForEntity<People>()
                .ForEntity<Request>()
                .ForEntity<Transaction>()
                .ForEntity<RequestTransaction>();
            });

            services.AddSingleton<IDbConnectionBuilder, DbConnectionBuilder>();
            services.AddScoped<IDbConnectionManager, DbConnectionManager>();

            #region Managers
            services.AddScoped<IPeopleManager, PeopleManager>();
            services.AddScoped<IRequestManager, RequestManager>();
            services.AddScoped<ITransactionManager, TransactionManager>();
            services.AddScoped<IRequestTransactionManager, RequestTransactionManager>();
            #endregion
            #region Stores
            services.AddScoped<IPeopleStore, PeopleStore>();
            services.AddScoped<IRequestStore, RequestStore>();
            services.AddScoped<ITransactionStore, TransactionStore>();
            services.AddScoped<IRequestTransactionStore, RequestTransactionStore>();
            services.AddScoped<IAppSettingsManager, AppSettingsManager>();
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRequestLogging();
            app.UseStaticFiles();
            app.UseMvc();
            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();
            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Smart Education Paycom API");
            });
            app.UseBasicAuth();
            // Add external authentication middleware below. To configure them please see https://go.microsoft.com/fwlink/?LinkID=532715
            app.UseJsonRpc();
        }
    }
}
